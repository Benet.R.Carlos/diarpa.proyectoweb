﻿''' <summary>
''' Clase para almacenar variables de Pagina
''' </summary>
Public Class C
    'URL
    Public Const URL_INICIO As String = "../Web/VistaProducto"
    Public Const URL_GALERIA As String = "../Web/Galeria"
    Public Const URL_VISTAPRODUCTO As String = "../Web/VistaProducto"
    Public Const URL_CATALOGO As String = "../Web/Catalogo"
    Public Const URL_CONTACTO As String = "../Web/Contacto"


    'Directorio para Imagenes
    Public Const DIR_IMG_DIM01 As String = "../img/product/single-product/"
    Public Const DIR_IMG_DIM02 As String = "../img/product/single-product/"
    Public Const DIR_IMG_DIM03 As String = "../img/product/single-product/"

    'nombres de variables para Session
    Public Const C_PRODUCTO_ID As String = "C_PRODUCTO_ID"
    Public Const C_TEMPORADA_ID As String = "C_TEMPORADA_ID"

    'Valores constantes
    Public Const DEFAULT_INTEGER As Integer = -1
    Public Const DEFAULT_DECIMAL As Integer = -1



End Class
