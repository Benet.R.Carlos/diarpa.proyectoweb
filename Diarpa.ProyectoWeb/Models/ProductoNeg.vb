﻿Imports System.Data.SqlClient

''' <summary>
''' Class ProductoNeg
''' </summary>
Public Class ProductoNeg

    ''' <summary>
    ''' Cadena de conexión a la base de datos
    ''' </summary>
    Private CadenaConexion As String

    ''' <summary>
    ''' New
    ''' </summary>
    Sub New()
        CadenaConexion = ConfigurationManager.ConnectionStrings("ConexionBD").ConnectionString
    End Sub

    ''' <summary>
    ''' InsertProducto
    ''' </summary>
    ''' <param name="oProducto">Objeto Producto a insertar.</param>
    ''' <returns>Si es correcto, objeto incertado en Producto, de otra forma Nothing.</returns>
    Public Function InsertProducto(ByVal oProducto As Producto) As Producto
        Dim cmd As SqlCommand = New SqlCommand
        Try
            cmd.Connection = New SqlConnection(CadenaConexion)
            cmd.Connection.Open()
            cmd.Transaction = cmd.Connection.BeginTransaction
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_Producto]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 1

            ' Table parameters:
            cmd.Parameters.Add("@VIdProducto", SqlDbType.Int).Value = C.DEFAULT_INTEGER
            cmd.Parameters.Add("@VIdCAT_Temporada", SqlDbType.Int).Value = oProducto.IdCAT_Temporada
            cmd.Parameters.Add("@VNombre", SqlDbType.NVarChar, 500).Value = IIf(String.IsNullOrEmpty(oProducto.Nombre), DBNull.Value, oProducto.Nombre)
            cmd.Parameters.Add("@Descripcion", SqlDbType.NVarChar, 500).Value = IIf(String.IsNullOrEmpty(oProducto.Descripcion), DBNull.Value, oProducto.Descripcion)
            cmd.Parameters("@VIdProducto").Direction = ParameterDirection.InputOutput
            cmd.ExecuteNonQuery()

            ' Get/Set the insertedId.
            Dim insertedId As Integer = CInt(cmd.Parameters("@VIdProducto").Value)
            If insertedId = 0 Then
                Throw New Exception("The last inserted record was not found.")
            Else
                oProducto.IdProducto = insertedId
            End If

            ' Comit.
            cmd.Transaction.Commit()

            ' Return
            Return oProducto
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return Nothing
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

    ''' <summary>
    ''' UpdateProducto
    ''' </summary>
    ''' <param name="oProducto">Objeto Producto para actualizar.</param>
    ''' <returns>Si es correcto, bandera True, de otra forma False.</returns>
    Public Function UpdateProducto(ByVal oProducto As Producto) As Boolean
        Dim cmd As SqlCommand = New SqlCommand
        Try

            cmd.Connection = New SqlConnection(CadenaConexion)
            cmd.Connection.Open()
            cmd.Transaction = cmd.Connection.BeginTransaction
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_Producto]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 2

            ' Table parameters:
            cmd.Parameters.Add("@VIdProducto", SqlDbType.Int).Value = oProducto.IdProducto
            cmd.Parameters.Add("@VIdCAT_Temporada", SqlDbType.Int).Value = oProducto.IdCAT_Temporada
            cmd.Parameters.Add("@VNombre", SqlDbType.NVarChar, 500).Value = IIf(String.IsNullOrEmpty(oProducto.Nombre), DBNull.Value, oProducto.Nombre)
            cmd.Parameters.Add("@Descripcion", SqlDbType.NVarChar, 500).Value = IIf(String.IsNullOrEmpty(oProducto.Descripcion), DBNull.Value, oProducto.Descripcion)
            cmd.ExecuteNonQuery()

            ' Comit.
            cmd.Transaction.Commit()

            ' Return
            Return True
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return False
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

    ''' <summary>
    ''' DeleteProducto
    ''' </summary>
    ''' <param name="IdProducto">Identificador del objeto Producto a eliminar.</param>
    ''' <returns>Si es correcto, bandera True, de otra forma False.</returns>
    Public Function DeleteProducto(ByVal IdProducto As Integer) As Boolean
        Dim cmd As SqlCommand = New SqlCommand
        Try

            cmd.Connection = New SqlConnection(CadenaConexion)
            cmd.Connection.Open()
            cmd.Transaction = cmd.Connection.BeginTransaction
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_Producto]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 3

            ' Table parameters:
            cmd.Parameters.Add("@VIdProducto", SqlDbType.Int).Value = IdProducto
            cmd.ExecuteNonQuery()

            ' Comit.
            cmd.Transaction.Commit()

            ' Return
            Return True
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return False
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

    ''' <summary>
    ''' GetProductoById
    ''' </summary>
    ''' <param name="IdProducto">Identificador del objeto Producto a obtener.</param>
    ''' <returns>Si es correcto, objeto encontrado en Producto, de otra forma Nothing.</returns>
    Public Function GetProductoById(ByVal IdProducto As Integer) As Producto
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter()
        Dim dt As DataTable = New DataTable
        Try

            cmd.Connection = New SqlConnection(CadenaConexion)
            cmd.Connection.Open()
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_Producto]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 4

            ' Table parameters:
            cmd.Parameters.Add("@VIdProducto", SqlDbType.Int).Value = IdProducto
            cmd.ExecuteNonQuery()

            ' Fill dt
            With da
                .SelectCommand = cmd
                .Fill(dt)
            End With

            Dim listOfProducto As New List(Of Producto)
            listOfProducto = (From Row In dt.Rows Select New Producto With {.IdProducto = Row("IdProducto"),
                                                                            .IdCAT_Temporada = Row("IdCAT_Temporada"),
                                                                            .Nombre = If(Row("Nombre") Is DBNull.Value, String.Empty, Row("Nombre")),
                                                                            .Descripcion = If(Row("Descripcion") Is DBNull.Value, String.Empty, Row("Descripcion"))
                                                                            }).ToList

            If listOfProducto IsNot Nothing Then
                If listOfProducto.Count > 0 Then
                    Dim oProducto As Producto = listOfProducto.First
                    ' Return
                    Return oProducto
                End If
            End If

            ' Return
            Return Nothing
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return Nothing
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

    ''' <summary>
    ''' GetProducto
    ''' </summary>
    ''' <returns>Si es correcto, todos los objetos en List(Of Producto), de otra forma Nothing.</returns>
    Public Function GetProducto() As List(Of Producto)
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter()
        Dim dt As DataTable = New DataTable
        Try

            cmd.Connection = New SqlConnection(CadenaConexion)
            cmd.Connection.Open()
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_Producto]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 101

            ' Table parameters:
            cmd.ExecuteNonQuery()

            ' Fill dt
            With da
                .SelectCommand = cmd
                .Fill(dt)
            End With

            Dim listOfProducto As New List(Of Producto)
            listOfProducto = (From Row In dt.Rows Select New Producto With {.IdProducto = Row("IdProducto"),
                                                                            .IdCAT_Temporada = Row("IdCAT_Temporada"),
                                                                            .Nombre = If(Row("Nombre") Is DBNull.Value, String.Empty, Row("Nombre")),
                                                                            .Descripcion = If(Row("Descripcion") Is DBNull.Value, String.Empty, Row("Descripcion"))
                                                                            }).ToList

            If listOfProducto IsNot Nothing Then
                If listOfProducto.Count > 0 Then
                    ' Return
                    Return listOfProducto
                End If
            End If

            ' Return
            Return Nothing
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return Nothing
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

End Class