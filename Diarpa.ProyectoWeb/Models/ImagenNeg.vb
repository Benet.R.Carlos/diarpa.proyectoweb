﻿Imports System.Data.SqlClient

''' <summary>
''' Class ImagenNeg
''' </summary>
Public Class ImagenNeg

    ''' <summary>
    ''' Cadena de conexión a la base de datos
    ''' </summary>
    Private CadenaConexion As String

    ''' <summary>
    ''' New
    ''' </summary>
    Sub New()
        CadenaConexion = ConfigurationManager.ConnectionStrings("ConexionBD").ConnectionString
    End Sub

    ''' <summary>
    ''' InsertImagen
    ''' </summary>
    ''' <param name="oImagen">Objeto Imagen a insertar.</param>
    ''' <returns>Si es correcto, objeto incertado en Imagen, de otra forma Nothing.</returns>
    Public Function InsertImagen(ByVal oImagen As Imagen) As Imagen
        Dim cmd As SqlCommand = New SqlCommand
        Try
            cmd.Connection = New SqlConnection(CadenaConexion)
            cmd.Connection.Open()
            cmd.Transaction = cmd.Connection.BeginTransaction
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_Imagen]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 1

            ' Table parameters:
            cmd.Parameters.Add("@VIdImagen", SqlDbType.Int).Value = -1
            cmd.Parameters.Add("@VIdProducto", SqlDbType.Int).Value = If(oImagen.IdProducto < 0, DBNull.Value, oImagen.IdProducto)
            cmd.Parameters.Add("@VIdTamanio", SqlDbType.Int).Value = If(oImagen.IdTamanio < 0, DBNull.Value, oImagen.IdTamanio)
            cmd.Parameters.Add("@VUrlDim01", SqlDbType.NVarChar, 250).Value = If(String.IsNullOrEmpty(oImagen.UrlDim01), DBNull.Value, oImagen.UrlDim01)
            cmd.Parameters.Add("@VUrlDim02", SqlDbType.VarBinary).Value = If(String.IsNullOrEmpty(oImagen.UrlDim02), DBNull.Value, oImagen.UrlDim02)
            cmd.Parameters.Add("@VUrlDim03", SqlDbType.Decimal).Value = If(String.IsNullOrEmpty(oImagen.UrlDim03), DBNull.Value, oImagen.UrlDim03)
            cmd.ExecuteNonQuery()

            ' Get/Set the insertedId.
            Dim insertedId As Integer = CInt(cmd.Parameters("@VIdImagen").Value)
            If insertedId = 0 Then
                Throw New Exception("The last inserted record was not found.")
            Else
                oImagen.IdImagen = insertedId
            End If

            ' Comit.
            cmd.Transaction.Commit()

            ' Return
            Return oImagen
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return Nothing
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

    ''' <summary>
    ''' UpdateImagen
    ''' </summary>
    ''' <param name="oImagen">Objeto Imagen para actualizar.</param>
    ''' <returns>Si es correcto, bandera True, de otra forma False.</returns>
    Public Function UpdateImagen(ByVal oImagen As Imagen) As Boolean
        Dim cmd As SqlCommand = New SqlCommand
        Try

            cmd.Connection = New SqlConnection(CadenaConexion)
            cmd.Connection.Open()
            cmd.Transaction = cmd.Connection.BeginTransaction
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_Imagen]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 2

            ' Table parameters:
            cmd.Parameters.Add("@VIdImagen", SqlDbType.Int).Value = oImagen.IdImagen
            cmd.Parameters.Add("@VIdProducto", SqlDbType.Int).Value = If(oImagen.IdProducto < 0, DBNull.Value, oImagen.IdProducto)
            cmd.Parameters.Add("@VIdTamanio", SqlDbType.Int).Value = If(oImagen.IdTamanio < 0, DBNull.Value, oImagen.IdTamanio)
            cmd.Parameters.Add("@VUrlDim01", SqlDbType.NVarChar, 250).Value = If(String.IsNullOrEmpty(oImagen.UrlDim01), DBNull.Value, oImagen.UrlDim01)
            cmd.Parameters.Add("@VUrlDim02", SqlDbType.VarBinary).Value = If(String.IsNullOrEmpty(oImagen.UrlDim02), DBNull.Value, oImagen.UrlDim02)
            cmd.Parameters.Add("@VUrlDim03", SqlDbType.Decimal).Value = If(String.IsNullOrEmpty(oImagen.UrlDim03), DBNull.Value, oImagen.UrlDim03)
            cmd.ExecuteNonQuery()

            ' Comit.
            cmd.Transaction.Commit()

            ' Return
            Return True
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return False
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

    ''' <summary>
    ''' DeleteImagen
    ''' </summary>
    ''' <param name="IdImagen">Identificador del objeto Imagen a eliminar.</param>
    ''' <returns>Si es correcto, bandera True, de otra forma False.</returns>
    Public Function DeleteImagen(ByVal IdImagen As Integer) As Boolean
        Dim cmd As SqlCommand = New SqlCommand
        Try

            cmd.Connection = New SqlConnection(CadenaConexion)
            cmd.Connection.Open()
            cmd.Transaction = cmd.Connection.BeginTransaction
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_Imagen]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 3

            ' Table parameters:
            cmd.Parameters.Add("@VIdImagen", SqlDbType.Int).Value = IdImagen
            cmd.ExecuteNonQuery()

            ' Comit.
            cmd.Transaction.Commit()

            ' Return
            Return True
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return False
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

    ''' <summary>
    ''' GetImagenById
    ''' </summary>
    ''' <param name="IdImagen">Identificador del objeto Imagen a obtener.</param>
    ''' <returns>Si es correcto, objeto encontrado en Imagen, de otra forma Nothing.</returns>
    Public Function GetImagenById(ByVal IdImagen As Integer) As Imagen
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter()
        Dim dt As DataTable = New DataTable
        Try

            cmd.Connection = New SqlConnection(CadenaConexion)
            cmd.Connection.Open()
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_Imagen]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 4

            ' Table parameters:
            cmd.Parameters.Add("@VIdImagen", SqlDbType.Int).Value = IdImagen
            cmd.ExecuteNonQuery()

            ' Fill dt
            With da
                .SelectCommand = cmd
                .Fill(dt)
            End With

            Dim listOfImagen As New List(Of Imagen)
            listOfImagen = (From Row In dt.Rows Select New Imagen With {.IdImagen = If(Row("IdImagen") Is DBNull.Value, -1, Row("IdImagen")),
                                                                        .IdProducto = If(Row("IdProducto") Is DBNull.Value, -1, Row("IdProducto")),
                                                                        .IdTamanio = If(Row("IdTamanio") Is DBNull.Value, -1, Row("IdTamanio")),
                                                                        .UrlDim01 = If(Row("UrlDim01") Is DBNull.Value, Nothing, Row("UrlDim01")),
                                                                        .UrlDim02 = If(Row("UrlDim02") Is DBNull.Value, Nothing, Row("UrlDim02")),
                                                                        .UrlDim03 = If(Row("UrlDim03") Is DBNull.Value, Nothing, Row("UrlDim03"))
                                                                        }).ToList

            If listOfImagen IsNot Nothing Then
                If listOfImagen.Count > 0 Then
                    Dim oImagen As Imagen = listOfImagen.First
                    ' Return
                    Return oImagen
                End If
            End If

            ' Return
            Return Nothing
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return Nothing
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

    ''' <summary>
    ''' GetImagen
    ''' </summary>
    ''' <returns>Si es correcto, todos los objetos en List(Of Imagen), de otra forma Nothing.</returns>
    Public Function GetImagen() As List(Of Imagen)
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter()
        Dim dt As DataTable = New DataTable
        Try

            cmd.Connection = New SqlConnection(CadenaConexion)
            cmd.Connection.Open()
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_Imagen]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 101

            ' Table parameters:
            cmd.ExecuteNonQuery()

            ' Fill dt
            With da
                .SelectCommand = cmd
                .Fill(dt)
            End With

            Dim listOfImagen As New List(Of Imagen)
            listOfImagen = (From Row In dt.Rows Select New Imagen With {.IdImagen = If(Row("IdImagen") Is DBNull.Value, -1, Row("IdImagen")),
                                                                        .IdProducto = If(Row("IdProducto") Is DBNull.Value, -1, Row("IdProducto")),
                                                                        .IdTamanio = If(Row("IdTamanio") Is DBNull.Value, -1, Row("IdTamanio")),
                                                                        .UrlDim01 = If(Row("UrlDim01") Is DBNull.Value, Nothing, Row("UrlDim01")),
                                                                        .UrlDim02 = If(Row("UrlDim02") Is DBNull.Value, Nothing, Row("UrlDim02")),
                                                                        .UrlDim03 = If(Row("UrlDim03") Is DBNull.Value, Nothing, Row("UrlDim03"))
                                                                        }).ToList

            If listOfImagen IsNot Nothing Then
                If listOfImagen.Count > 0 Then
                    ' Return
                    Return listOfImagen
                End If
            End If

            ' Return
            Return Nothing
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return Nothing
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

End Class
