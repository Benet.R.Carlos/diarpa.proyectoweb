﻿Imports System.Data.SqlClient

''' <summary>
''' Class InformacionAdicionalNeg
''' </summary>
Public Class InformacionAdicionalNeg

    ''' <summary>
    ''' Cadena de conexión a la base de datos
    ''' </summary>
    Private CadenaConexion As String

    ''' <summary>
    ''' New
    ''' </summary>
    Sub New()
        CadenaConexion = ConfigurationManager.ConnectionStrings("ConexionBD").ConnectionString
    End Sub

    ''' <summary>
    ''' InsertInformacionAdicional
    ''' </summary>
    ''' <param name="oInformacionAdicional">Objeto InformacionAdicional a insertar.</param>
    ''' <returns>Si es correcto, objeto incertado en InformacionAdicional, de otra forma Nothing.</returns>
    Public Function InsertInformacionAdicional(ByVal oInformacionAdicional As InformacionAdicional) As InformacionAdicional
        Dim cmd As SqlCommand = New SqlCommand
        Try
            cmd.Connection = New SqlConnection(CadenaConexion)
            cmd.Connection.Open()
            cmd.Transaction = cmd.Connection.BeginTransaction
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_InformacionAdicional]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 1

            ' Table parameters:
            cmd.Parameters.Add("@VIdInformacionAdicional", SqlDbType.Int).Value = C.DEFAULT_INTEGER
            cmd.Parameters.Add("@VIdProducto", SqlDbType.Int).Value = oInformacionAdicional.IdProducto
            cmd.Parameters.Add("@VIdCAT_Tamanio", SqlDbType.Int).Value = oInformacionAdicional.IdCAT_Tamanio
            cmd.Parameters.Add("@VIdCAT_Material", SqlDbType.Int).Value = oInformacionAdicional.IdCAT_Material
            cmd.Parameters.Add("@VCostoVentaMinima", SqlDbType.Decimal).Value = oInformacionAdicional.CostoVentaMinima
            cmd.Parameters.Add("@VCostoVentaPublico", SqlDbType.Decimal).Value = oInformacionAdicional.CostoVentaPublico
            cmd.Parameters.Add("@VDescuento", SqlDbType.Decimal).Value = IIf(oInformacionAdicional.Descuento = C.DEFAULT_INTEGER, DBNull.Value, oInformacionAdicional.Descuento)
            cmd.Parameters.Add("@VLongitudTita", SqlDbType.Int).Value = IIf(oInformacionAdicional.LongitudTita = C.DEFAULT_INTEGER, DBNull.Value, oInformacionAdicional.LongitudTita)
            cmd.Parameters.Add("@VLargo", SqlDbType.Int).Value = IIf(oInformacionAdicional.Largo = C.DEFAULT_INTEGER, DBNull.Value, oInformacionAdicional.Largo)
            cmd.Parameters.Add("@VAncho", SqlDbType.Int).Value = IIf(oInformacionAdicional.Ancho = C.DEFAULT_INTEGER, DBNull.Value, oInformacionAdicional.Ancho)
            cmd.Parameters.Add("@VPeso", SqlDbType.Int).Value = IIf(oInformacionAdicional.Peso = C.DEFAULT_INTEGER, DBNull.Value, oInformacionAdicional.Peso)
            cmd.Parameters.Add("@VEmpaquetado", SqlDbType.Int).Value = IIf(oInformacionAdicional.Empaquetado = C.DEFAULT_INTEGER, DBNull.Value, oInformacionAdicional.Empaquetado)
            cmd.Parameters.Add("@VDetalles", SqlDbType.NVarChar, 500).Value = IIf(String.IsNullOrEmpty(oInformacionAdicional.Detalles), DBNull.Value, oInformacionAdicional.Detalles)
            cmd.ExecuteNonQuery()

            ' Get/Set the insertedId.
            Dim insertedId As Integer = CInt(cmd.Parameters("@VIdInformacionAdicional").Value)
            If insertedId = 0 Then
                Throw New Exception("The last inserted record was not found.")
            Else
                oInformacionAdicional.IdInformacionAdicional = insertedId
            End If

            ' Comit.
            cmd.Transaction.Commit()

            ' Return
            Return oInformacionAdicional
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return Nothing
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

    ''' <summary>
    ''' UpdateInformacionAdicional
    ''' </summary>
    ''' <param name="oInformacionAdicional">Objeto InformacionAdicional para actualizar.</param>
    ''' <returns>Si es correcto, bandera True, de otra forma False.</returns>
    Public Function UpdateInformacionAdicional(ByVal oInformacionAdicional As InformacionAdicional) As Boolean
        Dim cmd As SqlCommand = New SqlCommand
        Try

            cmd.Connection = New SqlConnection(CadenaConexion)
            cmd.Connection.Open()
            cmd.Transaction = cmd.Connection.BeginTransaction
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_InformacionAdicional]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 2

            ' Table parameters:
            cmd.Parameters.Add("@VIdInformacionAdicional", SqlDbType.Int).Value = oInformacionAdicional.IdInformacionAdicional
            cmd.Parameters.Add("@VIdProducto", SqlDbType.Int).Value = oInformacionAdicional.IdProducto
            cmd.Parameters.Add("@VIdCAT_Tamanio", SqlDbType.Int).Value = oInformacionAdicional.IdCAT_Tamanio
            cmd.Parameters.Add("@VIdCAT_Material", SqlDbType.Int).Value = oInformacionAdicional.IdCAT_Material
            cmd.Parameters.Add("@VCostoVentaMinima", SqlDbType.Decimal).Value = oInformacionAdicional.CostoVentaMinima
            cmd.Parameters.Add("@VCostoVentaPublico", SqlDbType.Decimal).Value = oInformacionAdicional.CostoVentaPublico
            cmd.Parameters.Add("@VDescuento", SqlDbType.Decimal).Value = IIf(oInformacionAdicional.Descuento = C.DEFAULT_INTEGER, DBNull.Value, oInformacionAdicional.Descuento)
            cmd.Parameters.Add("@VLongitudTita", SqlDbType.Int).Value = IIf(oInformacionAdicional.LongitudTita = C.DEFAULT_INTEGER, DBNull.Value, oInformacionAdicional.LongitudTita)
            cmd.Parameters.Add("@VLargo", SqlDbType.Int).Value = IIf(oInformacionAdicional.Largo = C.DEFAULT_INTEGER, DBNull.Value, oInformacionAdicional.Largo)
            cmd.Parameters.Add("@VAncho", SqlDbType.Int).Value = IIf(oInformacionAdicional.Ancho = C.DEFAULT_INTEGER, DBNull.Value, oInformacionAdicional.Ancho)
            cmd.Parameters.Add("@VPeso", SqlDbType.Int).Value = IIf(oInformacionAdicional.Peso = C.DEFAULT_INTEGER, DBNull.Value, oInformacionAdicional.Peso)
            cmd.Parameters.Add("@VEmpaquetado", SqlDbType.Int).Value = IIf(oInformacionAdicional.Empaquetado = C.DEFAULT_INTEGER, DBNull.Value, oInformacionAdicional.Empaquetado)
            cmd.Parameters.Add("@VDetalles", SqlDbType.NVarChar, 500).Value = IIf(String.IsNullOrEmpty(oInformacionAdicional.Detalles), DBNull.Value, oInformacionAdicional.Detalles)
            cmd.Parameters("@VIdInformacionAdicional").Direction = ParameterDirection.InputOutput
            cmd.ExecuteNonQuery()

            ' Comit.
            cmd.Transaction.Commit()

            ' Return
            Return True
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return False
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

    ''' <summary>
    ''' DeleteInformacionAdicional
    ''' </summary>
    ''' <param name="IdInformacionAdicional">Identificador del objeto InformacionAdicional a eliminar.</param>
    ''' <returns>Si es correcto, bandera True, de otra forma False.</returns>
    Public Function DeleteInformacionAdicional(ByVal IdInformacionAdicional As Integer) As Boolean
        Dim cmd As SqlCommand = New SqlCommand
        Try

            cmd.Connection = New SqlConnection(CadenaConexion)
            cmd.Connection.Open()
            cmd.Transaction = cmd.Connection.BeginTransaction
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_InformacionAdicional]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 3

            ' Table parameters:
            cmd.Parameters.Add("@VIdInformacionAdicional", SqlDbType.Int).Value = IdInformacionAdicional
            cmd.ExecuteNonQuery()

            ' Comit.
            cmd.Transaction.Commit()

            ' Return
            Return True
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return False
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

    ''' <summary>
    ''' GetInformacionAdicionalById
    ''' </summary>
    ''' <param name="IdInformacionAdicional">Identificador del objeto InformacionAdicional a obtener.</param>
    ''' <returns>Si es correcto, objeto encontrado en InformacionAdicional, de otra forma Nothing.</returns>
    Public Function GetInformacionAdicionalById(ByVal IdInformacionAdicional As Integer) As InformacionAdicional
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter()
        Dim dt As DataTable = New DataTable
        Try

            cmd.Connection = New SqlConnection(CadenaConexion)
            cmd.Connection.Open()
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_InformacionAdicional]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 4

            ' Table parameters:
            cmd.Parameters.Add("@VIdInformacionAdicional", SqlDbType.Int).Value = IdInformacionAdicional
            cmd.ExecuteNonQuery()

            ' Fill dt
            With da
                .SelectCommand = cmd
                .Fill(dt)
            End With

            Dim listOfInformacionAdicional As New List(Of InformacionAdicional)
            listOfInformacionAdicional = (From Row In dt.Rows Select New InformacionAdicional With {.IdInformacionAdicional = Row("IdInformacionAdicional"),
                                                                                                    .IdProducto = Row("IdProducto"),
                                                                                                    .IdCAT_Tamanio = Row("IdCAT_Tamanio"),
                                                                                                    .IdCAT_Material = Row("IdCAT_Material"),
                                                                                                    .CostoVentaMinima = Row("CostoVentaMinima"),
                                                                                                    .CostoVentaPublico = Row("CostoVentaPublico"),
                                                                                                    .Descuento = If(Row("Descuento") Is DBNull.Value, C.DEFAULT_DECIMAL, Row("Descuento")),
                                                                                                    .LongitudTita = If(Row("LongitudTita") Is DBNull.Value, C.DEFAULT_INTEGER, Row("LongitudTita")),
                                                                                                    .Largo = If(Row("Largo") Is DBNull.Value, C.DEFAULT_INTEGER, Row("Largo")),
                                                                                                    .Ancho = If(Row("Ancho") Is DBNull.Value, C.DEFAULT_INTEGER, Row("Ancho")),
                                                                                                    .Peso = If(Row("Peso") Is DBNull.Value, C.DEFAULT_INTEGER, Row("Peso")),
                                                                                                    .Empaquetado = If(Row("Empaquetado") Is DBNull.Value, C.DEFAULT_INTEGER, Row("Empaquetado")),
                                                                                                    .Detalles = If(Row("Detalles") Is DBNull.Value, String.Empty, Row("Detalles"))
                                                                                                }).ToList

            If listOfInformacionAdicional IsNot Nothing Then
                If listOfInformacionAdicional.Count > 0 Then
                    Dim oInformacionAdicional As InformacionAdicional = listOfInformacionAdicional.First
                    ' Return
                    Return oInformacionAdicional
                End If
            End If

            ' Return
            Return Nothing
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return Nothing
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

    ''' <summary>
    ''' GetInformacionAdicional
    ''' </summary>
    ''' <returns>Si es correcto, todos los objetos en List(Of InformacionAdicional), de otra forma Nothing.</returns>
    Public Function GetInformacionAdicional() As List(Of InformacionAdicional)
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter()
        Dim dt As DataTable = New DataTable
        Try

            cmd.Connection = New SqlConnection(CadenaConexion)
            cmd.Connection.Open()
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_InformacionAdicional]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 101

            ' Table parameters:
            cmd.ExecuteNonQuery()

            ' Fill dt
            With da
                .SelectCommand = cmd
                .Fill(dt)
            End With

            Dim listOfInformacionAdicional As New List(Of InformacionAdicional)
            listOfInformacionAdicional = (From Row In dt.Rows Select New InformacionAdicional With {.IdInformacionAdicional = Row("IdInformacionAdicional"),
                                                                                                    .IdProducto = Row("IdProducto"),
                                                                                                    .IdCAT_Tamanio = Row("IdCAT_Tamanio"),
                                                                                                    .IdCAT_Material = Row("IdCAT_Material"),
                                                                                                    .CostoVentaMinima = Row("CostoVentaMinima"),
                                                                                                    .CostoVentaPublico = Row("CostoVentaPublico"),
                                                                                                    .Descuento = If(Row("Descuento") Is DBNull.Value, C.DEFAULT_DECIMAL, Row("Descuento")),
                                                                                                    .LongitudTita = If(Row("LongitudTita") Is DBNull.Value, C.DEFAULT_INTEGER, Row("LongitudTita")),
                                                                                                    .Largo = If(Row("Largo") Is DBNull.Value, C.DEFAULT_INTEGER, Row("Largo")),
                                                                                                    .Ancho = If(Row("Ancho") Is DBNull.Value, C.DEFAULT_INTEGER, Row("Ancho")),
                                                                                                    .Peso = If(Row("Peso") Is DBNull.Value, C.DEFAULT_INTEGER, Row("Peso")),
                                                                                                    .Empaquetado = If(Row("Empaquetado") Is DBNull.Value, C.DEFAULT_INTEGER, Row("Empaquetado")),
                                                                                                    .Detalles = If(Row("Detalles") Is DBNull.Value, String.Empty, Row("Detalles"))
                                                                                                }).ToList

            If listOfInformacionAdicional IsNot Nothing Then
                If listOfInformacionAdicional.Count > 0 Then
                    ' Return
                    Return listOfInformacionAdicional
                End If
            End If

            ' Return
            Return Nothing
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return Nothing
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

End Class