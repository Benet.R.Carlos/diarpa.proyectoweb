﻿Imports System.Data.SqlClient

''' <summary>
''' ElementoCatalogoNeg
''' </summary>
Public Class ElementoCatalogoNeg

    ''' <summary>
    ''' Cadena de conexión a la base de datos
    ''' </summary>
    Private CadenaConexion As String

    ''' <summary>
    ''' New
    ''' </summary>
    Sub New()
        CadenaConexion = ConfigurationManager.ConnectionStrings("ConexionBD").ConnectionString
    End Sub

    ''' <summary>
    ''' GetElementoCatalogoByTemporada
    ''' </summary>
    ''' <param name="IdCAT_Temporada">temporada a buscar</param>
    ''' <returns>Si es correcto, todos los objetos en List(Of ElementoCatalogo), de otra forma Nothing.</returns>
    Public Function GetElementoCatalogoByTemporada(ByVal IdCAT_Temporada As Integer) As List(Of ElementoCatalogo)
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter()
        Dim dt As DataTable = New DataTable
        Try

            cmd.Connection = New SqlConnection(CadenaConexion)
            cmd.Connection.Open()
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_ElementoCatalogo]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 150

            ' Table parameters:
            cmd.Parameters.Add("@VIdCAT_Temporada", SqlDbType.Int).Value = IdCAT_Temporada
            cmd.ExecuteNonQuery()

            ' Fill dt
            With da
                .SelectCommand = cmd
                .Fill(dt)
            End With

            Dim listOfElementoCatalogo As New List(Of ElementoCatalogo)
            listOfElementoCatalogo = (From Row In dt.Rows Select New ElementoCatalogo With {.IdProducto = Row("IdProducto"),
                                                                .IdCAT_Temporada = Row("IdCAT_Temporada"),
                                                                .Nombre = If(Row("Nombre") Is DBNull.Value, String.Empty, Row("Nombre")),
                                                                .UrlImagen = If(Row("UrlImagen") Is DBNull.Value, String.Empty, Row("UrlImagen")),
                                                                .PrecioMaximo = If(Row("PrecioMaximo") Is DBNull.Value, String.Empty, Row("PrecioMaximo")),
                                                                .PrecioMinimo = If(Row("PrecioMinimo") Is DBNull.Value, String.Empty, Row("PrecioMinimo"))
                                                                }).ToList

            If listOfElementoCatalogo IsNot Nothing Then
                If listOfElementoCatalogo.Count > 0 Then
                    ' Return
                    Return listOfElementoCatalogo
                End If
            End If

            ' Return
            Return Nothing
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return Nothing
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function


End Class
