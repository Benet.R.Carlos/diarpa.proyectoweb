﻿''' <summary>
''' Class Producto
''' </summary>
<Serializable()>
Public Class Producto
    Public Property IdProducto As Integer
    Public Property IdCAT_Temporada As Integer
    Public Property Nombre As String
    Public Property Descripcion As String


    Sub New()
        Me.IdProducto = C.DEFAULT_INTEGER
        Me.IdCAT_Temporada = C.DEFAULT_INTEGER
        Me.Nombre = String.Empty
        Me.Descripcion = String.Empty
    End Sub

    Sub New(ByVal oProducto As Producto)
        With oProducto
            Me.IdProducto = .IdProducto
            Me.IdCAT_Temporada = .IdCAT_Temporada
            Me.Nombre = .Nombre
            Me.Descripcion = .Descripcion
        End With
    End Sub

End Class