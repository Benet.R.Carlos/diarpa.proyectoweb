﻿Imports System.Data.SqlClient

''' <summary>
''' CAT_Tamanio
''' </summary>
Public Class CAT_Tamanio
    Public Property IdCAT_Tamanio As Integer
    Public Property CodigoTamanio As String

    Sub New()
        Me.IdCAT_Tamanio = C.DEFAULT_INTEGER
        Me.CodigoTamanio = String.Empty
    End Sub

    Sub New(ByVal oCAT_Tamanio As CAT_Tamanio)
        With oCAT_Tamanio
            Me.IdCAT_Tamanio = .IdCAT_Tamanio
            Me.CodigoTamanio = .CodigoTamanio

        End With
    End Sub

    ''' <summary>
    ''' GetCAT_Tamanio
    ''' </summary>
    ''' <param name="IdProducto">Identificador del objeto Producto a obtener.</param>
    ''' <returns>Si es correcto, todos los objetos en List(Of CAT_Tamanio), de otra forma Nothing.</returns>
    Public Shared Function GetCAT_Tamanio(ByVal IdProducto As Integer) As List(Of CAT_Tamanio)
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter()
        Dim dt As DataTable = New DataTable
        Try

            cmd.Connection = New SqlConnection(ConfigurationManager.ConnectionStrings("ConexionBD").ConnectionString)
            cmd.Connection.Open()
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_CAT_Tamanio]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 102

            ' Table parameters:
            cmd.ExecuteNonQuery()

            ' Fill dt
            With da
                .SelectCommand = cmd
                .Fill(dt)
            End With

            Dim listOfCAT_Tamanio As New List(Of CAT_Tamanio)
            listOfCAT_Tamanio = (From Row In dt.Rows Select New CAT_Tamanio With {.IdCAT_Tamanio = Row("IdCAT_Tamanio"),
                                                         .CodigoTamanio = Row("CodigoTamanio")
                                                         }).ToList

            If listOfCAT_Tamanio IsNot Nothing Then
                If listOfCAT_Tamanio.Count > 0 Then
                    ' Return
                    Return listOfCAT_Tamanio
                End If
            End If

            ' Return
            Return Nothing
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return Nothing
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

End Class
