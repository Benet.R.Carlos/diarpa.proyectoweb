﻿''' <summary>
''' Class InformacionAdicional
''' </summary>
<Serializable()>
Public Class InformacionAdicional

    Public Property IdInformacionAdicional As Integer
    Public Property IdProducto As Integer
    Public Property IdCAT_Tamanio As Integer
    Public Property IdCAT_Material As Integer
    Public Property CostoVentaMinima As Decimal
    Public Property CostoVentaPublico As Decimal
    Public Property Descuento As Decimal
    Public Property LongitudTita As Integer
    Public Property Largo As Integer
    Public Property Ancho As Integer
    Public Property Peso As Integer
    Public Property Empaquetado As Integer
    Public Property Detalles As String


    Sub New()
        Me.IdInformacionAdicional = C.DEFAULT_INTEGER
        Me.IdProducto = C.DEFAULT_INTEGER
        Me.IdCAT_Tamanio = C.DEFAULT_INTEGER
        Me.IdCAT_Material = C.DEFAULT_INTEGER
        Me.CostoVentaMinima = C.DEFAULT_DECIMAL
        Me.CostoVentaPublico = C.DEFAULT_DECIMAL
        Me.Descuento = C.DEFAULT_DECIMAL
        Me.LongitudTita = C.DEFAULT_INTEGER
        Me.Largo = C.DEFAULT_INTEGER
        Me.Ancho = C.DEFAULT_INTEGER
        Me.Peso = C.DEFAULT_INTEGER
        Me.Empaquetado = C.DEFAULT_INTEGER
        Me.Detalles = String.Empty
    End Sub

    Sub New(ByVal oInformacionGeneral As InformacionAdicional)
        With oInformacionGeneral
            Me.IdInformacionAdicional = .IdInformacionAdicional
            Me.IdProducto = .IdProducto
            Me.IdCAT_Tamanio = .IdCAT_Tamanio
            Me.IdCAT_Material = .IdCAT_Material
            Me.CostoVentaMinima = .CostoVentaMinima
            Me.CostoVentaPublico = .CostoVentaPublico
            Me.Descuento = .Descuento
            Me.LongitudTita = .LongitudTita
            Me.Largo = .Largo
            Me.Ancho = .Ancho
            Me.Peso = .Peso
            Me.Empaquetado = .Empaquetado
            Me.Detalles = .Detalles
        End With
    End Sub

End Class