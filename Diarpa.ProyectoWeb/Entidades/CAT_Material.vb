﻿Imports System.Data.SqlClient

''' <summary>
''' CAT_Material
''' </summary>
Public Class CAT_Material
    Public Property IdCAT_Material As Integer
    Public Property Material As String

    Sub New()
        Me.IdCAT_Material = C.DEFAULT_INTEGER
        Me.Material = String.Empty
    End Sub

    Sub New(ByVal oCAT_Material As CAT_Material)
        With oCAT_Material
            Me.IdCAT_Material = .IdCAT_Material
            Me.Material = .Material

        End With
    End Sub

    ''' <summary>
    ''' GetCAT_MaterialById
    ''' </summary>
    ''' <param name="IdCAT_Material">Identificador del objeto CAT_Material a obtener.</param>
    ''' <returns>Si es correcto, objeto encontrado en CAT_Material, de otra forma Nothing.</returns>
    Public Shared Function GetCAT_MaterialById(ByVal IdCAT_Material As Integer) As CAT_Material
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter()
        Dim dt As DataTable = New DataTable
        Try

            cmd.Connection = New SqlConnection(ConfigurationManager.ConnectionStrings("ConexionBD").ConnectionString)
            cmd.Connection.Open()
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_CAT_Material]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 4

            ' Table parameters:
            cmd.Parameters.Add("@VIdCAT_Material", SqlDbType.Int).Value = IdCAT_Material
            cmd.ExecuteNonQuery()

            ' Fill dt
            With da
                .SelectCommand = cmd
                .Fill(dt)
            End With

            Dim listOfCAT_Material As New List(Of CAT_Material)
            listOfCAT_Material = (From Row In dt.Rows Select New CAT_Material With {.IdCAT_Material = Row("IdCAT_Material"),
                                                                                    .Material = Row("Material")
                                                                                    }).ToList

            If listOfCAT_Material IsNot Nothing Then
                If listOfCAT_Material.Count > 0 Then
                    Dim oCAT_Material As CAT_Material = listOfCAT_Material.First
                    ' Return
                    Return oCAT_Material
                End If
            End If

            ' Return
            Return Nothing
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return Nothing
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

End Class
