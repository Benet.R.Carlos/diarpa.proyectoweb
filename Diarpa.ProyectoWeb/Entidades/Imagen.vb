﻿''' <summary>
''' Class Imagen
''' </summary>
<Serializable()>
Public Class Imagen
    Public Property IdImagen As Integer
    Public Property IdProducto As Integer
    Public Property IdTamanio As Integer
    Public Property UrlDim01 As String
    Public Property UrlDim02 As String
    Public Property UrlDim03 As String

    Sub New()
        Me.IdImagen = -1
        Me.IdProducto = -1
        Me.IdTamanio = -1
        Me.UrlDim01 = Nothing
        Me.UrlDim02 = Nothing
        Me.UrlDim03 = Nothing
    End Sub

    Sub New(ByVal oImagen As Imagen)
        With oImagen
            Me.IdImagen = .IdImagen
            Me.IdProducto = .IdProducto
            Me.IdTamanio = .IdTamanio
            Me.UrlDim01 = .UrlDim01
            Me.UrlDim02 = .UrlDim02
            Me.UrlDim03 = .UrlDim03
        End With
    End Sub

End Class