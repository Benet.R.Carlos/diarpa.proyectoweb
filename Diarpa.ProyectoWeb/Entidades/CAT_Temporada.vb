﻿Imports System.Data.SqlClient

Public Class CAT_Temporada

    Public Property IdCAT_Temporada As Integer
    Public Property Nombre As String
    Public Property Descripcion As String

    Sub New()
        Me.IdCAT_Temporada = C.DEFAULT_INTEGER
        Me.Nombre = String.Empty
        Me.Descripcion = String.Empty
    End Sub

    Sub New(ByVal oCAT_Temporada As CAT_Temporada)
        With oCAT_Temporada
            Me.IdCAT_Temporada = .IdCAT_Temporada
            Me.Nombre = .Nombre
            Me.Descripcion = .Descripcion
        End With
    End Sub

    ''' <summary>
    ''' GetCAT_TemporadaById
    ''' </summary>
    ''' <param name="IdCAT_Temporada">Identificador del objeto CAT_Temporada a obtener.</param>
    ''' <returns>Si es correcto, objeto encontrado en CAT_Temporada, de otra forma Nothing.</returns>
    Public Shared Function GetCAT_TemporadaById(ByVal IdCAT_Temporada As Integer) As CAT_Temporada
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter()
        Dim dt As DataTable = New DataTable
        Try

            cmd.Connection = New SqlConnection(ConfigurationManager.ConnectionStrings("ConexionBD").ConnectionString)
            cmd.Connection.Open()
            cmd.CommandTimeout = 0

            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[dbo].[SP_CAT_Temporada]"
            cmd.Parameters.Clear()

            ' Control parameters:
            cmd.Parameters.Add("@opcion", SqlDbType.TinyInt).Value = 4

            ' Table parameters:
            cmd.Parameters.Add("@VIdCAT_Temporada", SqlDbType.Int).Value = IdCAT_Temporada
            cmd.ExecuteNonQuery()

            ' Fill dt
            With da
                .SelectCommand = cmd
                .Fill(dt)
            End With

            Dim listOfCAT_Temporada As New List(Of CAT_Temporada)
            listOfCAT_Temporada = (From Row In dt.Rows Select New CAT_Temporada With {.IdCAT_Temporada = Row("IdCAT_Temporada"),
                                                                            .Nombre = If(Row("Nombre") Is DBNull.Value, String.Empty, Row("Nombre")),
                                                                            .Descripcion = If(Row("Descripcion") Is DBNull.Value, String.Empty, Row("Descripcion"))
                                                                            }).ToList

            If listOfCAT_Temporada IsNot Nothing Then
                If listOfCAT_Temporada.Count > 0 Then
                    Dim oCAT_Temporada As CAT_Temporada = listOfCAT_Temporada.First
                    ' Return
                    Return oCAT_Temporada
                End If
            End If

            ' Return
            Return Nothing
        Catch ex As Exception
            If cmd.Transaction IsNot Nothing Then
                cmd.Transaction.Rollback()
            End If
            ' Return
            Return Nothing
        Finally
            If cmd IsNot Nothing Then
                If cmd.Connection IsNot Nothing Then
                    If cmd.Connection.State <> ConnectionState.Closed Then
                        cmd.Connection.Close()
                    End If
                    cmd.Connection.Dispose()
                End If
                cmd.Dispose()
            End If
        End Try
    End Function

End Class