﻿''' <summary>
''' Class ElementoCatalogo
''' </summary>
<Serializable()>
Public Class ElementoCatalogo
    Public Property IdProducto As Integer
    Public Property IdCAT_Temporada As Integer
    Public Property Nombre As String
    Public Property UrlImagen As String
    Public Property PrecioMaximo As String
    Public Property PrecioMinimo As String

    Sub New()
        Me.IdProducto = C.DEFAULT_INTEGER
        Me.IdCAT_Temporada = C.DEFAULT_INTEGER
        Me.Nombre = String.Empty
        Me.UrlImagen = String.Empty
        Me.PrecioMaximo = String.Empty
        Me.PrecioMinimo = String.Empty
    End Sub

    Sub New(ByVal oElementoCatalogo As ElementoCatalogo)
        With oElementoCatalogo
            Me.IdProducto = .IdProducto
            Me.IdCAT_Temporada = .IdCAT_Temporada
            Me.Nombre = .Nombre
            Me.UrlImagen = .UrlImagen
            Me.PrecioMaximo = .PrecioMaximo
            Me.PrecioMinimo = .PrecioMinimo
        End With
    End Sub

    'Public Sub AjustarVista()
    '    Me.Nombre = "<h4>" & Me.Nombre & "</h4>"
    '    Me.UrlImagen = "../img/product/inspired-product/" & Me.UrlImagen & ".jpg"
    '    If Me.PrecioMaximo IsNot Nothing And Me.PrecioMinimo IsNot Nothing Then
    '        Me.PrecioMaximo = "$" & Me.PrecioMinimo & " a $" & Me.PrecioMaximo & "<br/><h4>(precio varia por tamaño)</h4>"
    '    End If
    'End Sub

    Public Shared Sub AjustarVista(ByRef oListElementoCatalogo As List(Of ElementoCatalogo))
        If oListElementoCatalogo IsNot Nothing AndAlso oListElementoCatalogo.Count > 0 Then
            For Each oElementoCatalogo In oListElementoCatalogo
                With oElementoCatalogo
                    .Nombre = "<h4>" & .Nombre & "</h4>"
                    .UrlImagen = "../img/product/inspired-product/" & .UrlImagen & ".jpg"
                    If Not String.IsNullOrEmpty(.PrecioMaximo) And Not String.IsNullOrEmpty(.PrecioMinimo) Then
                        .PrecioMaximo = "$" & .PrecioMinimo & " a $" & .PrecioMaximo & "<br/><h4>(precio varia por tamaño)</h4>"
                    End If
                End With
            Next
        End If
    End Sub

End Class