﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="VistaProducto.aspx.vb" Inherits="Diarpa.ProyectoWeb.VistaProducto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel runat="server" ID="uPnPrducto">
        <ContentTemplate>
            <!--================Single Product Area =================-->
            <div class="product_image_area">
                <div class="container">
                    <div class="row s_product_inner">
                        <div class="col-lg-6">
                            <div class="s_product_img">

                                <asp:Image ID="imgProducto" runat="server" CssClass="d-block w-100" />

                            </div>
                        </div>
                        <div class="col-lg-5 offset-lg-1">
                            <div class="s_product_text">

                                <asp:Label runat="server" ID="lblNombre" Text="<h3>Nombre</h3>" />
                                <asp:Label runat="server" ID="lblPrecio" Text="<h2>$00.00</h2>" />

                                <asp:DropDownList runat="server" ID="ddlTamanio" AutoPostBack="true" CssClass="dropdown">
                                    <asp:ListItem Text="Tamaño 01" Value="01" />
                                    <asp:ListItem Text="Tamaño 02" Value="02" />
                                </asp:DropDownList>

                                <br />
                                <br />
                                <br />

                                <ul class="list">
                                    <li>
                                        <asp:Label runat="server" ID="lblTemporada" class="active" Text="<span>Temporada:</span> Temporada" />
                                    </li>
                                    <li>
                                        <asp:Label runat="server" ID="lblTamanio" class="active" Text="<span>Tamaño:</span> Tamaño" />
                                    </li>
                                </ul>

                                <p>
                                    <asp:Label runat="server" ID="lblDescripcionCorta" class="align-text-bottom" Text="Descripción corta" />
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--================End Single Product Area =================-->

            <!--================Product Description Area =================-->
            <section class="product_description_area">
                <div class="container">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review" aria-selected="true">Revición general</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Especificaciones</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">

                        <div class="tab-pane fade sow active" id="review" role="tabpanel" aria-labelledby="review-tab">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="row total_rate">
                                        <div class="col-6">
                                            <div class="box_total">
                                                <h5>Calificación</h5>
                                                <h4>4.0</h4>
                                                <h6>(03 Reviews)</h6>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="rating_list">
                                                <h3>Based on 3 Reviews</h3>
                                                <ul class="list">
                                                    <li>
                                                        <a href="#">5 Star<i class="fa fa-star"></i>
                                                            <i class="fa fa-star-half-empty"></i>
                                                            <i class="fa fa-star-half-o"></i>
                                                            <i class="fa fa-star-half"></i>
                                                            <i class="fa fa-star"></i>01</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="review_list">
                                        <div class="review_item">
                                            <div class="media">
                                                <div class="d-flex">
                                                    <img
                                                        src="img/product/single-product/review-1.png"
                                                        alt="" />
                                                </div>
                                                <div class="media-body">
                                                    <h4>Blake Ruiz</h4>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                            </div>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                  sed do eiusmod tempor incididunt ut labore et dolore magna
                  aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                  ullamco laboris nisi ut aliquip ex ea commodo
                                       
                                            </p>
                                        </div>
                                        <div class="review_item">
                                            <div class="media">
                                                <div class="d-flex">
                                                    <img
                                                        src="img/product/single-product/review-2.png"
                                                        alt="" />
                                                </div>
                                                <div class="media-body">
                                                    <h4>Blake Ruiz</h4>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                            </div>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                  sed do eiusmod tempor incididunt ut labore et dolore magna
                  aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                  ullamco laboris nisi ut aliquip ex ea commodo
                                       
                                            </p>
                                        </div>
                                        <div class="review_item">
                                            <div class="media">
                                                <div class="d-flex">
                                                    <img
                                                        src="img/product/single-product/review-3.png"
                                                        alt="" />
                                                </div>
                                                <div class="media-body">
                                                    <h4>Blake Ruiz</h4>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                            </div>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                  sed do eiusmod tempor incididunt ut labore et dolore magna
                  aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                  ullamco laboris nisi ut aliquip ex ea commodo
                                       
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="review_box">
                                        <h4>Add a Review</h4>
                                        <p>Your Rating:</p>
                                        <ul class="list">
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                            </li>
                                        </ul>
                                        <p>Outstanding</p>
                                        <form
                                            class="row contact_form"
                                            action="contact_process.php"
                                            method="post"
                                            id="contactForm"
                                            novalidate="novalidate">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input
                                                        type="text"
                                                        class="form-control"
                                                        id="name"
                                                        name="name"
                                                        placeholder="Your Full name" />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input
                                                        type="email"
                                                        class="form-control"
                                                        id="email"
                                                        name="email"
                                                        placeholder="Email Address" />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input
                                                        type="text"
                                                        class="form-control"
                                                        id="number"
                                                        name="number"
                                                        placeholder="Phone Number" />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea
                                                        class="form-control"
                                                        name="message"
                                                        id="message"
                                                        rows="1"
                                                        placeholder="Review"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12 text-right">
                                                <button
                                                    type="submit"
                                                    value="submit"
                                                    class="btn submit_btn">
                                                    Submit Now
                                           
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <h5>Longitud de tira</h5>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblLongitud" class="align-text-bottom" Text="<h5>Longitud de tira</h5>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h5>Largo</h5>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblLargo" class="align-text-bottom" Text="<h5>Largo</h5>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h5>Ancho</h5>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblAncho" class="align-text-bottom" Text="<h5>Ancho</h5>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h5>Peso</h5>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblPeso" class="align-text-bottom" Text="<h5>Peso</h5>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h5>Tamaño</h5>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblTamanios" class="align-text-bottom" Text="<h5>Tamaño</h5>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h5>Material</h5>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblMaterial" class="align-text-bottom" Text="<h5>Material</h5>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h5>Empaquetado</h5>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblEmpaquetado" class="align-text-bottom" Text="<h5>Empaquetado</h5>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h5>Detalles</h5>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblDetalles" class="align-text-bottom" Text="<h5>Detalles</h5>" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <!--================End Product Description Area =================-->
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="ddlTamanio" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>