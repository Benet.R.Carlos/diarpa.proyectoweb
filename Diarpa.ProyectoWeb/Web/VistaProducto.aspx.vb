﻿Public Class VistaProducto
    Inherits System.Web.UI.Page

    ''' <summary>
    ''' Page_Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ''Al no contener referencia de un producto regresa al catálogo
        'If Session(C.C_PRODUCTO_ID) IsNot Nothing Then
        '    Response.Redirect("")
        'End If

        'CargaVistaProducto(oProducto, oInformacionAdicional, oImagen)
        If Not Page.IsPostBack Then
            CargaVistaProducto()
        End If
    End Sub

    ''' <summary>
    ''' CargaVistaProducto
    ''' </summary>
    '''  Private Sub CargaVistaProducto(ByVal oProducto As Producto, ByVal oInformacionAdicional As InformacionAdicional, ByVal oImagen As Imagen)
    Private Sub CargaVistaProducto()
        'Id producto que viene de catálogo.
        Dim IdProducto As Integer = IIf(Session(C.C_PRODUCTO_ID) IsNot Nothing, Session(C.C_PRODUCTO_ID), 1)
        'Recuperar datos
        Dim oProductoNeg As New ProductoNeg
        Dim oProducto As Producto = oProductoNeg.GetProductoById(IdProducto)
        Dim oInformacionAdicionalNeg As New InformacionAdicionalNeg
        Dim oInformacionAdicional As InformacionAdicional = oInformacionAdicionalNeg.GetInformacionAdicionalById(IdProducto)
        Dim oImagenNeg As New ImagenNeg
        Dim oImagen As Imagen = oImagenNeg.GetImagenById(IdProducto)

        If oProducto IsNot Nothing And oInformacionAdicional IsNot Nothing Then

            'Especificaciones
            lblLongitud.Text = "----"
            lblLargo.Text = "----"
            lblAncho.Text = "----"
            lblPeso.Text = "----"
            lblMaterial.Text = "----"
            lblEmpaquetado.Text = "----"
            lblDetalles.Text = "----"
            lblTamanio.Text = "----"

            'Alimentar vista
            imgProducto.ImageUrl = C.DIR_IMG_DIM01 & "s-product-1.jpg"
            lblNombre.Text = "<h3>" & oProducto.Nombre & "</h3>"
            lblPrecio.Visible = False

            Dim oListCAT_Tamanio As New List(Of CAT_Tamanio)

            oListCAT_Tamanio = CAT_Tamanio.GetCAT_Tamanio(IdProducto)
            lblTamanios.Text = String.Empty
            ddlTamanio.Items.Clear()
            ddlTamanio.Items.Add(New ListItem("Tamaños disponibles", "NullNothing"))
            For Each oCAT_Tamanio In oListCAT_Tamanio
                lblTamanios.Text = lblTamanios.Text & " " & oCAT_Tamanio.CodigoTamanio
                ddlTamanio.Items.Add(New ListItem(oCAT_Tamanio.CodigoTamanio, oCAT_Tamanio.IdCAT_Tamanio))
            Next
            Dim oCAT_Temporada As New CAT_Temporada
            oCAT_Temporada = CAT_Temporada.GetCAT_TemporadaById(oProducto.IdCAT_Temporada)
            lblTemporada.Text = "<span>Temporada:</span> " & oCAT_Temporada.Nombre
            lblTamanio.Visible = False
            lblDescripcionCorta.Text = oProducto.Descripcion

        Else

        End If
    End Sub

    Private Sub Limpiar()

    End Sub

    Private Sub ddlTamanio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTamanio.SelectedIndexChanged


        If ddlTamanio.SelectedValue <> "NullNoting" Then
            'Id producto que viene de catálogo.
            Dim IdProducto As Integer = IIf(Session(C.C_PRODUCTO_ID) IsNot Nothing, Session(C.C_PRODUCTO_ID), 1)
            'Recuperar datos
            Dim oProductoNeg As New ProductoNeg
            Dim oProducto As Producto = oProductoNeg.GetProductoById(IdProducto)
            Dim oInformacionAdicionalNeg As New InformacionAdicionalNeg
            Dim oInformacionAdicional As InformacionAdicional = oInformacionAdicionalNeg.GetInformacionAdicionalById(IdProducto)
            Dim oImagenNeg As New ImagenNeg
            Dim oImagen As Imagen = oImagenNeg.GetImagenById(IdProducto)

            'Alimentar vista
            lblPrecio.Visible = True
            lblPrecio.Text = "<h2>$" & oInformacionAdicional.CostoVentaPublico & "</h2>"
            lblTamanio.Visible = True
            lblTamanio.Text = "<span>Tamaño:</span> " & ddlTamanio.SelectedItem.Text


            'Especificaciones
            lblLongitud.Text = oInformacionAdicional.LongitudTita & " cm"
            lblLargo.Text = oInformacionAdicional.Largo & " cm"
            lblAncho.Text = oInformacionAdicional.Ancho & " cm"
            lblPeso.Text = oInformacionAdicional.Peso & " gm"

            Dim oCAT_Material As CAT_Material = CAT_Material.GetCAT_MaterialById(oInformacionAdicional.IdCAT_Material)
            lblMaterial.Text = oCAT_Material.Material
            lblEmpaquetado.Text = oInformacionAdicional.Empaquetado
            lblDetalles.Text = oInformacionAdicional.Detalles

        Else
            lblPrecio.Visible = False
            lblTamanio.Visible = False
        End If
    End Sub
End Class