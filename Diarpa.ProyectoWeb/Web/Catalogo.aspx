﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Catalogo.aspx.vb" Inherits="Diarpa.ProyectoWeb.Catalogo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>

            <!--================Home Banner Area =================-->
            <section class="banner_area">
                <div class="banner_inner d-flex align-items-center">
                    <div class="container">
                        <div class="banner_content d-md-flex justify-content-between align-items-center">
                            <div class="mb-3 mb-md-0">
                                <h2>Shop Category</h2>
                                <p>Very us move be blessed multiply night</p>
                            </div>
                            <div class="page_link">
                                <a href="index.html">Home</a>
                                <a href="category.html">Shop</a>
                                <a href="category.html">Women Fashion</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--================End Home Banner Area =================-->

            <!--================Category Product Area =================-->
            <section class="cat_product_area section_gap">
                <div class="container">
                    <div class="row flex-row-reverse">
                        <div class="col-lg-9">

                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>

                                    <div class="product_top_bar">
                                        <div class="left_dorp">
                                            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="sorting" AutoPostBack="true">
                                                <asp:ListItem Text="Item A" Value="a" />
                                                <asp:ListItem Text="Item B" Value="b" />
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="DropDownList2" runat="server" CssClass="sorting" AutoPostBack="true">
                                                <asp:ListItem Text="Item 6" Value="6" />
                                                <asp:ListItem Text="Item 12" Value="12" />
                                                <asp:ListItem Text="Item 18" Value="18" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="latest_product_inner">
                                        <div class="row">

                                            <asp:Repeater ID="repElementoCatalogo" runat="server">
                                                <ItemTemplate>
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="single-product">
                                                            <div class="product-img">
                                                                <asp:Image ImageUrl='<% #Eval("UrlImagen") %>' CssClass="card-img" runat="server" ID="imgUrlImagen" />
                                                                <div class="p_icon">
                                                                    <asp:LinkButton runat="server" ID="lbtIdProducto" CommandName="Ver" CommandArgument='<% #Eval("IdProducto") %>'><i class="ti-heart"></i></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                            <div class="product-btm">
                                                                <div class="mt-3">
                                                                    <asp:Label Text='<% #Eval("PrecioMaximo") %>' runat="server" ID="lblPrecio" />
                                                                    <%--<asp:Label Text='<% #Eval("PrecioMaximo") %>' runat="server" ID="lblPrecio" />--%>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>

                                        </div>
                                    </div>


                                    <nav class="blog-pagination justify-content-center d-flex">
                                        <ul class="pagination">
                                            <li class="page-item">
                                                <a href="#" class="page-link" aria-label="Previous">
                                                    <span aria-hidden="true">
                                                        <span class="ti-arrow-left"></span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="page-item">
                                                <a href="#" class="page-link">...</a>
                                            </li>
                                            <li class="page-item active">
                                                <a href="#" class="page-link">1</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="#" class="page-link">2</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="#" class="page-link">3</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="#" class="page-link">...</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="#" class="page-link" aria-label="Next">
                                                    <span aria-hidden="true">
                                                        <span class="ti-arrow-right"></span>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>

                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="DropDownList1" />
                                    <asp:PostBackTrigger ControlID="DropDownList2" />
                                </Triggers>
                            </asp:UpdatePanel>



                        </div>

                        <div class="col-lg-3">
                            <div class="left_sidebar_area">
                                <aside class="left_widgets p_filter_widgets">
                                    <div class="l_w_title">
                                        <h3>Seccion Filtro #01</h3>
                                    </div>
                                    <div class="widgets_inner">
                                        <ul class="list">
                                            <li>
                                                <a href="#">Filtro #01</a>
                                            </li>
                                            <li>
                                                <a href="#">Filtro #02</a>
                                            </li>
                                            <li>
                                                <a href="#">Filtro #03</a>
                                            </li>
                                        </ul>
                                    </div>
                                </aside>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--================End Category Product Area =================-->
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>
