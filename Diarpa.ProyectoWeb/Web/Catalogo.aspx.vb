﻿Public Class Catalogo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            cargaCatalogo()
        End If
    End Sub

    Private Sub cargaCatalogo()
        'Id producto que viene de catálogo.
        Dim IdCAT_Temporada As Integer = IIf(Session(C.C_TEMPORADA_ID) IsNot Nothing, Session(C.C_TEMPORADA_ID), 1)

        Dim oListElementoCatalogo As New List(Of ElementoCatalogo)
        Dim oElementoCatalogoNeg As New ElementoCatalogoNeg

        oListElementoCatalogo = oElementoCatalogoNeg.GetElementoCatalogoByTemporada(IdCAT_Temporada)
        ElementoCatalogo.AjustarVista(oListElementoCatalogo)
        'For index = 1 To 9
        '    Dim oElementoCatalogo As New ElementoCatalogo
        '    With oElementoCatalogo
        '        .IdProducto = 2
        '        .UrlImagen = "../img/product/inspired-product/i4.jpg"
        '        .Nombre = "<h4>" & "Nombre " & index & "</h4>"
        '        .PrecioMaximo = "$ " & index * 10 & " - $50 <br/><h4>(precio varia por tamaño)</h4>"

        '    End With
        '    oListElementoCatalogo.Add(oElementoCatalogo)
        'Next

        repElementoCatalogo.DataSource = oListElementoCatalogo
        repElementoCatalogo.DataBind()
    End Sub

    ''' <summary>
    ''' repElementoCatalogo_ItemCommand
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    Private Sub repElementoCatalogo_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles repElementoCatalogo.ItemCommand
        Select Case e.CommandName
            Case "Ver"
                Session(C.C_PRODUCTO_ID) = e.CommandArgument
                Response.Redirect(C.URL_VISTAPRODUCTO)
        End Select
    End Sub

End Class